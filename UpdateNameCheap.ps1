# updateNameCheap.ps1 - Update NameCheap with our current IP address.

# Configure global variables
$HOSTNAME = 'hostname'
$DOMAINNAME = 'example.net'
$PASSWORD = '[put your dynamic dns password here]'
$DNSHOST = 'dynamicdns.park-your-domain.com'

# Get external IP
#$EXTERNIP = 127.0.0.1
$EXTERNIP = Invoke-WebRequest myexternalip.com/raw

# Update IP address
Invoke-WebRequest "https://$DNSHOST/update?host=$HOSTNAME&domain=$DOMAINNAME&password=$PASSWORD&ip=$EXTERNIP"
