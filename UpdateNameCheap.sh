#!/bin/bash
# update_NameCheap.sh - update NameCheap.com host information

# Configure global variables
HOSTNAME=$(hostname -s)
DOMAINNAME=$(hostname -d)
PASSWORD=''
DNSHOST='dynamicdns.park-your-domain.com'

IPSTORE='/var/lib/UpdateNameCheap/previousip'

# Get Previous IP Address
PREVIOUSIP=$(cat "$IPSTORE")

# Get external IP Address
EXTERNIP=$(curl -s "http://myexternalip.com/raw")

# Compare current IP to last IP
if [ "$EXTERNIP" != "$PREVIOUSIP" ]; then
	# Update IP address
	curl -s "https://$DNSHOST/update?host=$HOSTNAME&domain=$DOMAINNAME&password=$PASSWORD&ip=$EXTERNIP"
	
	if [ $? -eq 0 ]; then
		echo $EXTERNIP > "$IPSTORE"
	else
		echo UpdateNameCheep failed to update IP address.
	fi
fi

