# UpdateNameCheap (https://bitbucket.org/matthewlinton/updatenamecheap/)
Update NameCheap's Dynamic DNS servers with your public IP address.

## Files
NameCheap_update.ps1 : Powershell script to update DNS servers from within an Windows environment.

NameCheap_update.sh : Bash script to update DNS servers from within a Linux environment.

## Instructions
1. Download the latest version ( https://bitbucket.org/matthewlinton/updatenamecheap/downloads ).
1. Unzip the archive.
1. Navigate to the UpdateNameCheap directory.
1. Open the script that will run under your system.
1. Modify the configuration.
1. Run the script.

## Third Parties
UpdateNameCheap relies on some third parties to function correctly.

### My External IP (http://myexternalip.com/)
UpdateNameCheap uses My External IP's API to fetch the public IP address of the host that is updating it's IP.

## License
This code is not covered under any license. You are welcome to modify and share this code as you please.

## Liability
Use UpdateNameCheap at your own risk!

UpdateNameCheap, to the best of my knowledge, does not contain any harmful or malicious code. I assume no liability for any issues that may occur from the use of this software. Please take the time to understand how this code will interact with your system before using it.